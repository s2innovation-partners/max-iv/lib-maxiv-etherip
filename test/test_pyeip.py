"""
These tests depend on the following:

- pytest (testing framework)
- cpppo (PLC simulator)
- hypothesis (QuickCheck-alike test tool)

The are all pure Python and can be installed using pip.

Run the tests like this:

   $ py.test test/

Note: The run takes some time (a minute or so) because of the
hypothesis looping over the tests with varying inputs.
"""


from subprocess import Popen
import time

from cpppo.server.enip import client
import pytest
from pytest import raises
from hypothesis import given
from hypothesis.specifiers import integers_in_range, floats_in_range
import pyeip


PLCHOST = "localhost"
int_tags = ["INT_%d" % i for i in xrange(10)]
real_tags = ["REAL_%d" % i for i in xrange(10)]


# # #  FIXTURE  # # #

@pytest.yield_fixture(scope="module")
def plc(request):

    "Fake PLC fixture, runs a PLC simulator in a separate process"

    int_tag_args = ["%s=INT" % tag for tag in int_tags]
    real_tag_args = ["%s=REAL" % tag for tag in real_tags]
    process = Popen(["python", "-m", "cpppo.server.enip"] +
                    int_tag_args + real_tag_args)

    # make sure the server is up before yielding
    for x in xrange(50):
        try:
            conn = client.connector(host=PLCHOST, timeout=5)
            break
        except:
            time.sleep(0.2)
    else:
        pytest.xfail("Could not connect to fake PLC. "
                     "Is the cpppo module correctly installed?")

    yield conn  # the connection can be used to read/write tags

    # TESTS RUN HERE

    process.terminate()
    process.wait()


@pytest.yield_fixture()
def eip():

    "EtherIP library fixture"

    conn = setup_eip()

    # Create some scanlists:
    # * non-scanned ints
    conn.create_scanlist(int_tags[:1], period=0)
    # * fast-scanned ints
    conn.create_scanlist(int_tags[1:5], period=1.0)
    # * slow-scanned ints
    conn.create_scanlist(int_tags[5:6], 10.0)
    # * reals
    conn.create_scanlist(real_tags[1:5], 1.0)

    yield conn

    # TEST RUNS HERE

    conn.shutdown()


@pytest.yield_fixture()
def eip_all_tags_bad():
    "A setup where none of the tags are present on the PLC"
    conn = setup_eip()
    tags = ["A", "B", "C"]
    conn.create_scanlist(tags, period=0, complete=False)
    yield conn
    conn.shutdown


@pytest.yield_fixture()
def eip_some_tags_bad():
    "Some tags are OK, some are missing"
    conn = setup_eip()
    tags = ["INT_1", "REAL_2", "A", "B", "C"]
    conn.create_scanlist(tags, period=0, complete=False)
    yield conn
    conn.shutdown


# # #  Helpers  # # #

def setup_eip():
    conn = pyeip.Eip(PLCHOST)
    return conn


def write_tag(plc, tag, value, tol=1e-5):
    with plc:
        result = plc.synchronous(
            operations=client.parse_operations(["%s=%r" % (tag, value)]))
        for r in result:
            plc.validate(r)
    # wait until the read is completed
    while abs(read_tag(plc, tag) - value) > tol:
        time.sleep(0.1)


def read_tag(plc, tag):
    with plc:
        result = plc.synchronous(
            operations=client.parse_operations(["%s" % tag]))
        value = list(result)[-1][-1][0]
    return value


# # #  TESTS  # # #

# === Basic error checking

def test_connect_failure_raises():
    BADHOST = "FOOBAR"
    with raises(pyeip.PLCConnectionError):
        pyeip.Eip(BADHOST)


def test_is_connected(plc, eip):
    assert eip.is_connected()


# def test_get_tag_from_nonexistent_plc_raises(plc, eip):
#     BADPLC = "APA"
#     TAG = "INT_6"
#     with raises(pyeip.PLCConnectionError):
#         eip.get_tag(BADPLC, TAG)


# def test_set_tag_to_nonexistent_plc_raises(plc, eip):
#     BADPLC = "BEPA"
#     TAG = "REAL_2"
#     with raises(pyeip.PLCConnectionError):
#         eip.set_tag(BADPLC, TAG, 25.1)


# def test_get_tag_info_from_nonexistent_plc_raises(plc, eip):
#     BADPLC = "BEPA"
#     TAG = "REAL_2"
#     with raises(pyeip.PLCConnectionError):
#         eip.get_tag_info(BADPLC, TAG)


def test_get_nonexistent_tag_raises(plc, eip):
    BADTAG = "BAR"
    with pytest.raises(pyeip.TagMissingError):
        eip.get_tag(BADTAG)


def test_set_nonexistent_tag_raises(plc, eip):
    BADTAG = "FOO"
    with pytest.raises(pyeip.TagMissingError):
        eip.set_tag(BADTAG, 76)


def test_get_info_for_nonexistent_tag_raises(plc, eip):
    BADTAG = "FOO"
    with pytest.raises(pyeip.TagMissingError):
        eip.get_tag_info(BADTAG)


def test_scan_nonexistent_period_raises(plc, eip):
    PERIOD = 4.5
    with pytest.raises(pyeip.ScanListError):
        eip.scan(PERIOD)


def test_complete_scanlist_with_no_valid_tags_raises(plc, eip_all_tags_bad):
    with pytest.raises(pyeip.TagMissingError):
        eip_all_tags_bad.complete()


def test_complete_scanlist_with_some_invalid_tags_ok(plc, eip_some_tags_bad):
    eip_some_tags_bad.complete()


# === Reading tags ===

@given(n=integers_in_range(1, 4), x=integers_in_range(-32768, 32767))
def test_read_int(plc, eip, n, x):
    print n, x
    TAG = "INT_%d" % n
    write_tag(plc, TAG, x)  # write directly to the fake PLC
    eip.scan(1.0)  # run a scan
    assert eip.get_tag(TAG)[0] == x  # read back through pyeip


@given(n=integers_in_range(1, 4), x=floats_in_range(-1.1e38, 1.1e38))
def test_read_large_float(plc, eip, n, x):
    print x
    TAG = "REAL_%d" % n
    write_tag(plc, TAG, x, tol=1e100)  # write directly to the fake PLCHOST
    eip.scan(1.0)  # run a scan
    value = eip.get_tag(TAG)[0]  # read back through pyeip
    # Because the PLC real is 32 bit and Python floats are 64 bit, we have
    # to take care when comparing them. This could be done in a better way.
    assert abs(value - x) < 1e31  # primitive, I know...


@given(n=integers_in_range(1, 4), x=floats_in_range(-1, 1))
def test_read_medium_float(plc, eip, n, x):
    print x
    TAG = "REAL_%d" % n
    write_tag(plc, TAG, x)  # write directly to the fake PLC
    eip.scan(1.0)  # run a scan
    value = eip.get_tag(TAG)[0]  # read back through pyeip
    assert abs(value - x) < 1e-7


@given(n=integers_in_range(1, 4), x=floats_in_range(-1e-12, 1e-12))
def test_read_small_float(plc, eip, n, x):
    print x
    TAG = "REAL_%d" % n
    write_tag(plc, TAG, x)  # write directly to the fake PLC
    eip.scan(1.0)  # run a scan
    value = eip.get_tag(TAG)[0]  # read back through pyeip
    assert abs(value - x) < 1e-18


# === Writing tags ===

@given(n=integers_in_range(1, 4), x=integers_in_range(-32768, 32767))
def test_write_int(plc, eip, n, x):
    TAG = "INT_%d" % n
    eip.set_tag(TAG, x)  # write tag through pyeip
    eip.scan(1.0)
    readback = read_tag(plc, TAG)  # read it back from the fake PLC
    assert x == readback


@given(n=integers_in_range(1, 4), x=floats_in_range(-1.1e38, 1.1e38))
def test_write_large_float(plc, eip, n, x):
    TAG = "REAL_%d" % n
    eip.set_tag(TAG, x)
    eip.scan(1.0)
    readback = read_tag(plc, TAG)
    assert abs(x - readback) < 1e31


@given(n=integers_in_range(1, 4), x=floats_in_range(-1, 1))
def test_write_medium_float(plc, eip, n, x):
    TAG = "REAL_%d" % n
    eip.set_tag(TAG, x)
    eip.scan(1.0)
    readback = read_tag(plc, TAG)
    assert abs(x - readback) < 1e-7


@given(n=integers_in_range(1, 4), x=floats_in_range(-1e-12, 1e-12))
def test_write_small_float(plc, eip, n, x):
    TAG = "REAL_%d" % n
    eip.set_tag(TAG, x)
    eip.scan(1.0)
    readback = read_tag(plc, TAG)
    assert abs(x - readback) < 1e-19


# === Other stuff ===

def test_get_tag_info(plc, eip):
    TAG = "REAL_2"
    info = eip.get_tag_info(TAG)
    assert isinstance(info, dict)
    assert info["elements"] == 1
    assert info["tag"] == TAG
    assert info["period"] == 1.0
    # TODO: maybe assert some more


def test_get_scanlist_info(plc, eip):
    PERIOD = 1.0
    info = eip.get_scanlist_info(PERIOD)
    assert isinstance(info, dict)
    assert info["period"] == PERIOD
    assert info["list_errors"] == 0
    # ...
