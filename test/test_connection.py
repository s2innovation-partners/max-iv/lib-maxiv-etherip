from subprocess import Popen
import time
from contextlib import contextmanager, wraps

from cpppo.server.enip import client
from pytest import raises
import pyeip


@contextmanager
def plc_sim():

    "context with a simulated PLC in a process"

    tags = ["A=INT", "B=REAL", "C=DINT"]
    process = Popen(["python", "-m", "cpppo.server.enip"] + tags)

    yield connect_to_plc()

    process.terminate()
    process.wait()
    assert process.returncode is not None


def connect_to_plc():
    for x in xrange(50):
        try:
            conn = client.connector(host="localhost", timeout=5000)
            break
        except:
            time.sleep(0.2)
    else:
        return
    return conn


def write_tag(plc, tag, value, tol=1e-5):
    with plc:
        result = plc.synchronous(
            operations=client.parse_operations(["%s=%r" % (tag, value)]))
        for r in result:
            plc.validate(r)
    # wait until the read is completed
    while abs(read_tag(plc, tag) - value) > tol:
        time.sleep(0.1)


def read_tag(plc, tag):
    with plc:
        result = plc.synchronous(
            operations=client.parse_operations(["%s" % tag]))
        value = list(result)[-1][-1][0]
    return value


EIP_TIMEOUT = 1.0


def test_connection_stays_up_when_plc_goes_down():

    # start a PLC simulator
    with plc_sim() as conn:
        # assert client is not None
        e = pyeip.Eip(timeout=EIP_TIMEOUT)
        assert e.is_connected()
        e.create_scanlist(["A", "B", "C"])
    # simulator stopped

    time.sleep(EIP_TIMEOUT * 60)

    # check that we get errors when trying to read
    with raises(pyeip.PLCConnectionError):
        e.scan(0)

    # start PLC sim again
    with plc_sim() as conn:
        # change the tag directly on the PLC sim
        write_tag(conn, "A", 45)

        e.scan(0)
        # if this assertion is true, the scan must have succeeded!
        assert e.get_tag("A")[0] == 45
