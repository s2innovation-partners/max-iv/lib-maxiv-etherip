# example:
# LD_LIBRARY_PATH="/home/johfor/bygg/ether_ip/lib/linux-x86_64:." python test.py

from math import sin
import time
import random

import pyeip

# Note: the test PLC is changing sometimes (it's for testing), tags
# coming and going etc. This can result in errors, so that's something
# to check if this test script is failing.
TEST_PLC = "130.235.92.158"
TEST_SLOT = 10


def main(tag, period, amp):

    print tag, period, amp

    e = pyeip.Eip(TEST_PLC, slot=TEST_SLOT, verbosity=0)

    e.define_plc("hejsan", TEST_PLC, TEST_SLOT)
    e.add_tag("hejsan", tag, period, elements=1)
    e.complete("hejsan")
    e.dump()
    #e.scan("hejsan", 1)

    while True:
        e.scan("hejsan", period)
        time.sleep(period)
        value = int(sin(time.time() / 10) * amp)
        print tag, value
        e.set_tag("hejsan", tag, value)


if __name__ == "__main__":
    import sys
    main(sys.argv[1], float(sys.argv[2]), float(sys.argv[3]))
