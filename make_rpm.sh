#!/bin/bash

mkdir /tmp/installdir;
echo '/sbin/ldconfig' > /tmp/installdir/run-ldconfig.sh

mkdir /tmp/installdir/lib /tmp/installdir/python
cd ~/etherip/base-3.14.12.4/lib/linux-x86_64/
cp libasIoc.so.3.14 libdbIoc.so.3.14 librecIoc.so.3.14 libsoftDevIoc.so.3.14 libca.so.3.14 libdbStaticIoc.so.3.14 libregistryIoc.so.3.14 libCom.so.3.14 libdbtoolsIoc.so.3.14 libmiscIoc.so.3.14 librsrvIoc.so.3.14 /tmp/installdir/lib
cp ~/etherip/ether_ip/ether_ipApp/src/O.linux-x86_64/libether_ip.so /tmp/installdir/lib
cd ~/lib-maxiv-etherip
cp libeip.so /tmp/installdir/lib
cp pyeip.so /tmp/installdir/python

fpm -s dir -t rpm -n python-etherip -v 0.1 -C /tmp/installdir \
  --after-install /tmp/installdir/run-ldconfig.sh \
  lib=/usr python=/usr/lib/python2.6/site-packages
