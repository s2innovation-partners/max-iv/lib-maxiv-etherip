## PyEIP ##

This is a python API for the etherip C library from SNS, to talk to Allen-Bradley PLCs from Rockwell.

It consists of a slightly modified version of the EPICS driver from the above library (basically removes all EPICS specific functionality; mostly locks) and a Cython based python binding to this, handling connections and scanlists. Polling needs to be implemented manually on top of this if required.

The 'etherip' library (from http://ics-web.sns.ornl.gov/kasemir/etherip/) is also included, with some modifications (support for the LINT (64 bit integer) used for timestamps) is included in this project so the only external dependency to build is EPICS Base (http://aps.anl.gov/epics/download/base/index.php). When you have downloaded and built that for your platform, building and installing this module is as simple as e.g.:

```
EPICS_BASE=/opt/EPICS/base-3.14.12.5 python setup.py install --user
```


### Documentation ###

There is currently no real documentation. Look in 'test.py' to see some examples of how to use the library.


### Unit tests ###

There is some unit test coverage. The tests depend on the following packages:

- pytest (testing framework)
- cpppo (PLC simulator)
- hypothesis (QuickCheck-alike test tool)

The are all pure Python and can be installed using pip. Then run the tests like this:

```
$ py.test test/
```

Note: The run takes some time (a minute or so) because of the hypothesis looping over the tests with varying inputs.
