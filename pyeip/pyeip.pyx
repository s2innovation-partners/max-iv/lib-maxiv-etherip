import struct
import time

from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free

cimport eip

# data type signatures
T_CIP_BOOL   = 0x00C1
T_CIP_SINT   = 0x00C2
T_CIP_INT    = 0x00C3
T_CIP_DINT   = 0x00C4
T_CIP_LINT   = 0x00C5
T_CIP_REAL   = 0x00CA
T_CIP_BITS   = 0x00D3
T_CIP_STRUCT = 0x02A0


class EIPError(Exception):
    pass


class TagMissingError(EIPError):
    pass


class PLCConnectionError(EIPError):
    pass


class ScanListError(EIPError):
    pass


cdef class Eip:

    cdef eip.EIPConnection* connection
    cdef str ip
    cdef int port, slot, timeout
    cdef char* plcname

    def __cinit__(self, ip="localhost", port=44818, slot=0,
                  timeout=5, verbosity=0,
                  buffer_limit=500, char * plcname="plc"):
        # Confusion; you give an IP, port and slot here, but then you
        # can add PLCs independently using "define_plc", and without
        # running "connect()".  Not sure how this should be
        # handled. Perhaps IP etc should instead be arguments to
        # "connect()". If we even need it.
        self.ip = ip
        self.port = port
        self.slot = slot
        self.timeout = timeout
        self.plcname = plcname

        # Be very careful about changing this! It should probably
        # never be above 500.
        # Although supposedly, some PLC models have a setting to increase
        # this limit to ~5000 bytes. Worth looking into perhaps.
        eip.EIP_buffer_limit = buffer_limit

        # Not sure why this is needed, operation works without it, but
        # the "shutdown" method segfaults if this is left out.
        self.connection = eip.EIP_init()
        self.connect()

        self.define_plc(self.plcname, self.ip, self.slot)

        eip.EIP_verbosity = verbosity  # can be used to get debug printouts

    cpdef int is_connected(self):
        return bool(self.connection.sock)

    cpdef connect(self):
        "Open communication with PLC."
        success = eip.EIP_startup(self.connection, self.ip, self.port, self.slot,
                                  self.timeout*1000)
        if not success:
            raise PLCConnectionError("Could not make connection to PLC!")

    cpdef shutdown(self):
        "Close communication with PLC. It's unclear exactly what this does."
        if self.is_connected():
            eip.EIP_shutdown(self.connection)

    cpdef dump(self):
        "Debugging method"
        eip.drvEtherIP_dump()

    cdef eip.CN_UINT get_data_type(self, const unsigned char *data):
        cdef eip.CN_UINT type_ = data[0] | (data[1] << 8)  # find the type
        return type_

    cdef decode_data(self, const unsigned char* data, size_t elements):

        """Takes tag data and decodes it into a tuple of values"""

        cdef eip.CN_UINT type_ = data[0] | (data[1] << 8)  # find the type
        cdef const unsigned char* value = &data[2]

        # Note: probably pretty inefficient to use the python struct module
        # here since it means we're calling back to python..? There are decoding
        # functions in the library we can use. Otherwise, the decoding is pretty
        # simple anyway.
        if type_ == T_CIP_BOOL:
            return struct.unpack("%dB" % elements, value[:elements])
        if type_ == T_CIP_SINT:
            return struct.unpack("%dB" % elements, value[:elements])
        if type_ == T_CIP_INT:
            return struct.unpack("%dh" % elements, value[:elements*2])
        if type_ == T_CIP_DINT:
            return struct.unpack("%di" % elements, value[:elements*4])
        if type_ == T_CIP_LINT:
            return struct.unpack("%dq" % elements, value[:elements*8])
        if type_ == T_CIP_REAL:
            return struct.unpack("%df" % elements, value[:elements*4])
        # just to be sure...
        raise ValueError("Type {0} not recognized!".format(type_))

    def define_plc(self, name, ip="", slot=0):
        """Add a PLC to the internal list. Note that this is normally
        not needed unless you want to talk to more than one PLC from
        the same pyeip instance, which is not really supported anyway."""
        eip.drvEtherIP_define_PLC(name, ip if ip else self.ip,
                                  slot if slot else self.slot)
	# TODO: add a connection check here?

    def create_scanlist(self, tags, period=0, complete=True, scan=True):
        "convenience function to create an entire scanlist in one call"
        for tag in tags:
            if isinstance(tag, tuple):
                tag, elements = tag
            else:
                elements = 1
            self.add_tag(tag, period, elements)
        if complete:
            self.complete()
            if scan:
                self.scan(period)

    def add_tag(self, tagname, period=0, elements=1):
        "Add a tag to the scanlist with the given period"
        cdef eip.PLC *plc = eip.drvEtherIP_find_PLC(self.plcname)
        cdef eip.TagInfo* info = eip.drvEtherIP_add_tag(plc, period, tagname, elements)

    cpdef read_tag(self, const char *tagname, int elements, int timeout):
        """Read a single tag. Potentially inefficient since it uses a whole request to get
        one value. Also, just dumps the tag data to stdout. Clearly a debugging
        util."""
        eip.drvEtherIP_read_tag(self.ip, self.slot, tagname, elements, timeout)

    cpdef get_tag(self, const char *tagname):

        """Get the latest read value of a tag. Assumes that the tag has been
        added and a scan has been made."""

        cdef eip.PLC *plc = eip.drvEtherIP_find_PLC(self.plcname)
        if not plc:
            raise PLCConnectionError("PLC {0} not defined!".format(self.plcname))
        cdef eip.ScanList *scanlist
        cdef eip.TagInfo *taginfo
        if not plc:
            raise PLCConnectionError("PLC {0} not defined!".format(self.plcname))

        if (eip.find_PLC_tag(plc, tagname, &scanlist, &taginfo) and
            taginfo.data):
            return self.decode_data(taginfo.data, taginfo.elements)
        # Getting here probably means that the tag does not exist...
        raise TagMissingError("The tag '{0}' is not in any scanlist!"
                              .format(tagname))

    cpdef get_tag_info(self, const char *tagname):
        """Get the info of a tag. Assumes that the tag has been
        added and a complete() call has been made, otherwise the info will
        not be correct."""
        cdef eip.PLC *plc = eip.drvEtherIP_find_PLC(self.plcname)
        if not plc:
            raise PLCConnectionError("PLC {0} not defined!".format(self.plcname))
        cdef eip.ScanList *scanlist
        cdef eip.TagInfo *taginfo
        if (eip.find_PLC_tag(plc, tagname, &scanlist, &taginfo) and
            taginfo.data_size > 0):
            return {"tag": taginfo.string_tag,
                    "period": scanlist.period,
                    "type": self.get_data_type(taginfo.data),
                    "elements": taginfo.elements,
                    "cip_r_request_size": taginfo.cip_r_request_size,
                    "cip_r_response_size": taginfo.cip_r_response_size,
                    "cip_w_request_size": taginfo.cip_w_request_size,
                    "cip_w_response_size": taginfo.cip_w_response_size,
                    "data_size": taginfo.data_size,
                    "valid_data_size": taginfo.valid_data_size,
                    "do_write": taginfo.do_write,
                    "is_writing": taginfo.is_writing,
                    "transfer_time": taginfo.transfer_time}
        raise TagMissingError("The tag '{0}' is not in any scanlist!"
                              .format(tagname))

    cpdef get_scanlist_info(self, double period):
        cdef eip.PLC* plc = eip.drvEtherIP_find_PLC(self.plcname)
        if not plc:
            raise PLCConnectionError("PLC {0} not defined".format(self.plcname))
        if not eip.assert_PLC_connect(plc):
            raise PLCConnectionError("Problem connecting with PLC!")
        cdef eip.ScanList* scanlist = eip.get_PLC_ScanList(plc, period, False)
        if scanlist:
            return {"period": scanlist.period,
                    "list_errors": scanlist.list_errors,
                    "sched_errors": scanlist.sched_errors,  # used?
                    "min_scan_time": scanlist.min_scan_time,
                    "max_scan_time": scanlist.max_scan_time,
                    "last_scan_time": scanlist.last_scan_time}
        raise ScanListError("No scanlist with period {0}!".format(period, self.plcname))

    def set_tag(self, tagname, value, element=0):

        """Request that a tag be written the next time it's scanned."""
        cdef eip.PLC *plc = eip.drvEtherIP_find_PLC(self.plcname)
        cdef eip.ScanList *scanlist
        cdef eip.TagInfo *taginfo
        cdef eip.CN_UINT dtype

        if not plc:
            raise PLCConnectionError("PLC {0} not defined".format(self.plcname))

        if eip.find_PLC_tag(plc, tagname, &scanlist, &taginfo) and taginfo.data_size > 0:
            dtype = self.get_data_type(taginfo.data)
            if dtype == T_CIP_BOOL:
                result = eip.put_CIP_UDINT(taginfo.data, element, value)
            elif dtype == T_CIP_SINT:
                result = eip.put_CIP_UDINT(taginfo.data, element, value)
            elif dtype == T_CIP_INT:
                result = eip.put_CIP_DINT(taginfo.data, element, value)
            elif dtype == T_CIP_DINT:
                result = eip.put_CIP_DINT(taginfo.data, element, value)
            elif dtype == T_CIP_REAL:
                result = eip.put_CIP_double(taginfo.data, element, value)
            # getting False as a result means the type was somehow wrong;
            # perhaps raise an error instead?
            taginfo.do_write = result
            return result
        raise TagMissingError("The tag '{0}' is not in any scanlist!"
                              .format(tagname))

    def complete(self):
        """
        Fill in all the tag info (read/write sizes, etc) for a PLC. This
        needs to be done before running any scans. Returns True if at least
        one tag was successfully read.
        """
        cdef eip.PLC* plc = eip.drvEtherIP_find_PLC(self.plcname)
        if not plc:
            raise PLCConnectionError("PLC {0} not defined".format(self.plcname))
        if not eip.assert_PLC_connect(plc):
            # This is a bit confusing; the assert_PLC_connect call also calls
            # complete_PLC_Scanlist_TagInfos so assuming that the PLC is already
            # connected, an error must mean that there were no valid tags.
            raise TagMissingError("None of the tags could be found on the PLC!")

    cpdef scan(self, double period):
        """Wrapper for _scan, because we want to release the GIL and
        this can only be done in a cdef function, which can not raise
        exceptions."""
        cdef eip.PLC* plc
        plc = eip.drvEtherIP_find_PLC(self.plcname)
        if not plc:
            raise PLCConnectionError("No PLC defined!")
        if not eip.assert_PLC_connect(plc):
            raise PLCConnectionError("Not connected to PLC!")
        if not self._scan(plc, period):
            raise ScanListError("Error scanning scanlist with period {0}"
                                .format(period))

    cdef int _scan(self, eip.PLC *plc, double period):
        """Do a scan, meaning go through all the tags belonging to the scanlist
        corresponding to the given period, reading tags and writing any changes.
        """
        cdef eip.ScanList* scanlist
        # Release the GIL, since we're not manipulating any python objects.
        # This allows other threads in the program to work while scanning.
        with nogil:
            scanlist = eip.get_PLC_ScanList(plc, period, False)
            if scanlist:
                return eip.process_ScanList(plc.connection, scanlist)
