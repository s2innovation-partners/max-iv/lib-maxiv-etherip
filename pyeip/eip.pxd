cdef extern from "eip_driver.h":

    cdef int EIP_verbosity

    cdef int EIP_buffer_limit

    ctypedef signed char    CN_SINT
    ctypedef unsigned char  CN_USINT
    ctypedef unsigned short CN_UINT
    ctypedef short          CN_INT
    ctypedef unsigned int   CN_UDINT
    ctypedef int            CN_DINT
    ctypedef float          CN_REAL

    ctypedef int         EIP_SOCKET
    ctypedef int eip_bool

    ctypedef struct DLL_Node:
        pass

    ctypedef struct DL_List:
        pass

    # Connection

    ctypedef struct EIPConnectionParameters:
        CN_USINT   priority_and_tick
        CN_USINT   connection_timeout_ticks
        CN_UDINT   O2T_CID
        CN_UDINT   T2O_CID
        CN_UINT    connection_serial
        CN_UINT    vendor_ID
        CN_UDINT   originator_serial

    ctypedef struct EIPIdentityInfo:
        CN_UINT vendor
        CN_UINT device_type
        CN_UINT revision
        CN_UDINT serial_number
        CN_USINT name[100]

    ctypedef struct EIPConnection:
        EIP_SOCKET      sock
        int             slot
        size_t          transfer_buffer_limit
        size_t          millisec_timeout
        CN_UDINT        session
        CN_USINT        *buffer
        EIPIdentityInfo         info
        EIPConnectionParameters params

    void EIP_dump_connection(const EIPConnection *c)

    ctypedef enum type_:
        te_name,
        te_element
    ctypedef union value_:
        char   *name
        size_t element

    ctypedef struct ParsedTag:
        type_ type
        value_ value
        ParsedTag   *next

    ctypedef struct PLC:
        DLL_Node      node
        char          *name
        char          *ip_addr
        int           slot
        size_t        plc_errors
        size_t        slow_scans
        EIPConnection *connection
        DL_List       scanlists

    ctypedef struct ScanList:
        DLL_Node       node
        PLC            *plc
        eip_bool       enabled
        double         period
        size_t         list_errors
        size_t         sched_errors
        double         min_scan_time
        double         max_scan_time
        double         last_scan_time
        DL_List        taginfos

    ctypedef struct TagInfo:
        DLL_Node   node
        ScanList   *scanlist
        char       *string_tag
        ParsedTag  *tag
        size_t     elements
        size_t     cip_r_request_size
        size_t     cip_r_response_size
        size_t     cip_w_request_size
        size_t     cip_w_response_size
        size_t     data_size
        size_t     valid_data_size
        eip_bool   do_write
        eip_bool   is_writing
        CN_USINT   *data
        double     transfer_time
        DL_List    callbacks

    # connection
    EIPConnection* EIP_init()

    ctypedef int eip_bool
    ctypedef unsigned char CN_USINT

    eip_bool complete_PLC_ScanList_TagInfos(PLC *plc)

    eip_bool EIP_startup(EIPConnection* connection,
                         const char *ipaddress,
                         unsigned short port,
                         int slot, size_t ms_timeout)

    void EIP_shutdown(EIPConnection* connection)

    eip_bool assert_PLC_connect(PLC *plc)

    # Data read
    const unsigned char* EIP_read_tag(EIPConnection* connection,
                                      const ParsedTag* tag,
                                      size_t elements,
                                      size_t* data_size,
                                      size_t* request_size,
                                      size_t* response_size)

    int drvEtherIP_read_tag(const char *ip_addr,
                            int slot,
                            const char *tag_name,
                            int elements,
                            int timeout)

    TagInfo *find_ScanList_Tag (const ScanList *scanlist, const char *string_tag)

    void dump_TagInfo(const TagInfo *info, int level)

    void drvEtherIP_init()

    eip_bool drvEtherIP_define_PLC(const char *PLC_name,
                                   const char *ip_addr, int slot)


    PLC *drvEtherIP_find_PLC (const char *PLC_name) nogil

    eip_bool find_PLC_tag(PLC *plc,
                          const char *string_tag,
                          ScanList **list,
                          TagInfo **info)

    TagInfo *drvEtherIP_add_tag(PLC *plc, double period,
                                const char *string_tag, size_t elements)

    void drvEtherIP_dump ()

    eip_bool process_ScanList(EIPConnection *c, ScanList *scanlist) nogil

    ScanList *get_PLC_ScanList(PLC *plc, double period, eip_bool create) nogil

    const CN_USINT *unpack_UINT(const CN_USINT *buffer, CN_UINT *val)
    const CN_USINT *unpack_UDINT(const CN_USINT *buffer, CN_UDINT *val)
    const CN_USINT *unpack_REAL(const CN_USINT *buffer, CN_REAL *val)

    eip_bool get_CIP_double(const CN_USINT *raw_type_and_data,
                            size_t element, double *result)
    eip_bool get_CIP_DINT(const CN_USINT *raw_type_and_data,
                          size_t element, CN_DINT *result)
    eip_bool get_CIP_USINT(const CN_USINT *raw_type_and_data,
                           size_t element, CN_USINT *result)

    eip_bool put_CIP_double(const CN_USINT *raw_type_and_data,
                            size_t element, double value)
    eip_bool put_CIP_UDINT(const CN_USINT *raw_type_and_data,
                           size_t element, CN_UDINT value)
    eip_bool put_CIP_DINT(const CN_USINT *raw_type_and_data,
                  size_t element, CN_DINT value)
