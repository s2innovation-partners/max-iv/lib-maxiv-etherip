# example:
# LD_LIBRARY_PATH="/home/johfor/bygg/ether_ip/lib/linux-x86_64:." python test.py


import time
import random

import pyeip

# Note: the test PLC is changing sometimes (it's for testing), tags
# coming and going etc. This can result in errors, so that's something
# to check if this test script is failing.
TEST_PLC = "130.235.92.158"
TEST_SLOT = 10

TAGS = [("NodeTest", 1, 1),
        ("NodeBool", 1, 1),
        ("REAL_test", 1, 1),
        #("HVA_Test:C.DATA", 1, 400),
        ("Prod_to093004.Float", 1, 10),
        ("Prod_to093004.DATA", 1, 10),
        ("slask", 1, 1)]

print ""
print "*** Connecting and reading a tag"
e = pyeip.Eip(TEST_PLC, slot=TEST_SLOT, verbosity=0)
time.sleep(.1)
print "REAL_test", e.read_tag("REAL_test", 1, 1000)

print ""
print "*** Defining a PLC and adding some tags"
e.define_plc("hejsan", TEST_PLC, TEST_SLOT)
for tag, period, length in TAGS:
    e.add_tag("hejsan", tag, period=period, elements=length)


print ""
print "*** Completing the PLC (reading tag sizes)"
print e.complete("hejsan")

e.scan("hejsan", 1)

for tag, _, _ in TAGS:
    print e.get_tag_info("hejsan", tag)

#print e.get_tag_info("hejsan", "hjejoo")

print ""
print "*** Dumping tag data"
e.dump()

print ""
print "*** Performing a scan"
t0 = time.time()
e.scan("hejsan", 1)
print "scan took %f s" % (time.time() - t0)
for tag, _, _ in TAGS:
    print "get_tag", tag, e.get_tag("hejsan", tag)
# print "get_tag REAL_test", e.get_tag("hejsan", "REAL_test")
# print "get_tag", e.get_tag("hejsan", "Prod_to093004.DATA")
# print "get_tag", e.get_tag("hejsan", "HVA_Test:C.DATA")
# print "Flaot", e.get_tag("hejsan", "Prod_to093004.Float")

print ""
print "*** Writing a float tag and reading it back"
r = e.get_tag("hejsan", "REAL_test")
v = random.random()
print "random float:", v
e.set_tag("hejsan", "REAL_test", v, 0)
print "REAL_test", e.get_tag("hejsan", "REAL_test")

e.set_tag("hejsan", "slask", 0, 0)
e.set_tag("hejsan", "Prod_to093004.Float", 89.5, 3)

time.sleep(1.0)

print ""
print "*** Scanning and dumping all tag data again"
e.scan("hejsan", 1)
for tag, _, _ in TAGS:
    print "get_tag", tag, e.get_tag("hejsan", tag)
#print "REAL_test", e.read_tag("REAL_test", 1, 400)

#time.sleep(0.1)
#e.shutdown()
