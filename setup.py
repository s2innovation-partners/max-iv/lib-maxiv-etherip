"""
Building presumes that you've downloaded and compiled EPICS base
(http://www.aps.anl.gov/epics/base).

Use the environment variable EPICS_BASE to point to the
installation directory
"""

import os
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from subprocess import Popen, PIPE

EPICS_BASE = os.environ.get('EPICS_BASE', "/opt/EPICS/base")
EPICS_HOST_ARCH = os.environ.get("EPICS_HOST_ARCH")

if EPICS_HOST_ARCH is None:
    import platform
    if platform.machine() == 'x86_64':
        EPICS_HOST_ARCH = 'linux-x86_64'
    else:
        EPICS_HOST_ARCH = 'linux-x86'

pyeip_ext = Extension("pyeip",
                      sources = ["pyeip/pyeip.pyx",
                                 "pyeip/dl_list.c",
                                 "pyeip/ether_ip.c",
                                 "pyeip/eip_driver.c"],

                      define_macros = [('NO_EPICS', 1)],

                      include_dirs = ["pyeip",
                                      EPICS_BASE + "/include",
                                      EPICS_BASE + "/include/compiler/gcc",
                                      EPICS_BASE + "/include/os/Linux"],

                      # Statically compile any EPICS dependencies
                      # into the shared library
                      extra_objects = [EPICS_BASE + "/lib/" + EPICS_HOST_ARCH + "/libCom.a"],
                      libraries = ["stdc++", "rt", "readline"])

setup(
    name = 'python-etherip',
    version = '1.0.1',
    description = 'An Ethernet/IP library for Python',
    ext_modules = cythonize([pyeip_ext])
)
